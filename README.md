Листинг 1.7: Запуск Rails-сервера в cloud IDE.

$ cd ~/workspace/hello_app/
$ rails server -b $IP -p $PORT
--------
Листинг 1.8: Добавление действия hello в Application-контроллер. app/controllers/application_controller.rb

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def hello
    render text: "ch01_hello_app"
  end
end
----------
Листинг 1.10: Установка корневого маршрута. config/routes.rb 
  root 'application#hello'
--------------
